var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

var fs = require("fs");
var file = "wr.db";
var exists = fs.existsSync(file);

//Accessing sqlite db
var sqlite3 = require("sqlite3").verbose();
var db = new sqlite3.Database(file);
var sqlQuery = "";

app.use(express.static('public'));
server.listen(8080,function(){
	console.log("Listening on *:8080")
});


setInterval(e => wr.currentTimeAndAlarmChk(), 1000);

io.on('connection', function (socket) {
	socket.on("btnSave",function(data){
		wr.dbInsert(data)
	});

	//audiobuffer
	socket.on("sound",function(){
		io.emit("soundA")	
		console.log("click")
	})
	//
	

	socket.on("clientLoad",function(){
		wr.objPopulate(true) //init
		wr.currentTimeAndAlarmChk(true);
		io.emit("rdStObj",wr.rdStObj);
	})

	socket.on("dbDLt",function(){
		wr.dbDLt()
	});

	socket.on("dltAlrmFromDb",function(data){
		var dltAlrmFromDb = parseInt(data);
		console.log(dltAlrmFromDb)
		db.serialize(function() {
			sqlQuery = 'DELETE FROM alarms WHERE rowid="'+dltAlrmFromDb+'"';
			db.run(sqlQuery);
			//db.close();
			console.log("dltAlrmFromDb has completed")
		})
		wr.objPopulate(true);
	});

	socket.on("dbToggleAlrm",function(data){

		var key = parseInt(data)
		db.serialize(function() {
			var sqlQuerySel = 'SELECT ring FROM alarms WHERE rowid="'+key+'"'
			db.each(sqlQuerySel,function(err,row){
				//console.log(row.ring)
				switch(row.ring){
					case "true" : sqlQuery = 'UPDATE alarms SET ring = "false" WHERE rowid="'+key+'"';db.run(sqlQuery);wr.objPopulate(true);
					break;

					case "false" : sqlQuery = 'UPDATE alarms SET ring = "true" WHERE rowid="'+key+'"';db.run(sqlQuery);wr.objPopulate(true);
					break;

					//default : //Do nth
					//break;
				}
			})

			
			//db.close();
			console.log("toggling completed")
			
		})
	})

	socket.on("sendVolume",function (data) {
		console.log(data)
		wr.volume = data
		wr.sendVolume(wr.volume)
		//socket.emit('sendVolume',data);
		
	})

	socket.on('pause', function() {
        socket.broadcast.emit('pause');
    });

    socket.on('playBtn', function(playObj) {
        socket.broadcast.emit('playBtn',playObj);
        console.log("playBtn: "+playObj)
    });


	socket.on("volume",function(data){
		socket.broadcast.emit("volume",data)
	})

})

var wr = {

	//volInit = 42 dB approx avg with sound meter
	rdStObj : [
		{index : 0, rdSt : "p1", rdStURL : "http://live-icy.gss.dr.dk/A/A03L.mp3",volInit : 1}, 
		{index : 1, rdSt : "usInternetRadio", rdStURL :  "http://us1.internet-radio.com:8180", volInit : 1},//not working
		{index : 2, rdSt : "voice", rdStURL : "http://195.184.101.203/voice128",volInit : 0.2},
		{index : 3, rdSt : "Spring.mp3", rdStURL : "assets/music/zedge/spring.mp3",volInit : 0.2},
		{index : 4, rdSt : "Il Divo - La Fuerza Mayor .mp3", rdStURL : "assets/music/ilDivo/LaFuerzaMayor.mp3",volInit : 0.4},
	],


	outputMsg : function(msg){
		console.log(msg)
		io.emit("outputMsg",msg)
	},

	dbInsert : function(obj){
		//console.log(obj)
		//insrt must be in JSON format
		var matchCount = 0;
		var dbObj = {}
		//JSON stringify the following items before inserting in SQLITe. DB not handling obj and arr
		obj.days = JSON.stringify(obj.days)
		obj.repeat = JSON.stringify(obj.repeat)
		obj.rdStObj = JSON.stringify(obj.rdStObj)
		obj.clientDate = JSON.stringify(obj.clientDate)

		//console.log("GMT: " + obj.GMT)

		db.serialize(function() {
			//sqlQuery = 'CREATE TABLE IF NOT EXISTS alarms (hour INTEGER,min INTEGER,repeat TEXT, days TEXT,ring TEXT,rdSt TEXT,rdStURL TEXT, volRamp TEXT)';//rdStObj instead
			sqlQuery = 'CREATE TABLE IF NOT EXISTS alarms (hour INTEGER,min INTEGER,repeat TEXT, days TEXT,ring TEXT,rdStObj TEXT, volRamp TEXT, GMT INTEGER, clientDate TEXT)';//rdStObj instead
			db.run(sqlQuery);
			var stmt = db.prepare('INSERT INTO alarms VALUES (?,?,?,?,?,?,?,?,?)');

			sqlQuery = 'SELECT rowid AS id, * FROM alarms';
			
			db.each(sqlQuery,function(err,row){
				dbObj = {
					hour : row.hour,
					min : row.min,
					repeat : row.repeat,
					days : row.days,
					ring : row.ring,
					rdStObj : row.rdStObj,
					volRamp : row.volRamp,
					GMT : row.GMT,
					clientDate : row.clientDate
				}

				if(dbObj.hour == obj.hour && dbObj.min == obj.min){
					if(dbObj.days == obj.days){
						matchCount++
					}
				} 
			},function(){
				console.log("Callback after dbInsert")
				if(0 <= obj.hour && obj.hour < 24 && 0 <= obj.min && obj.min <= 60){
					if(matchCount == 0){
						stmt.run(
							obj.hour,
							obj.min,
							obj.repeat,
							obj.days,
							obj.ring,
							obj.rdStObj,
							obj.volRamp,
							obj.GMT, //calculated above
							obj.clientDate
						); //Insert data
						stmt.finalize();
					} else {
						var msg = "The alarm already exists and will not be stored"
						wr.outputMsg(msg)
					}
				} else {
					var msg = "not a valid time"
					wr.outputMsg(msg)
				}
			})
		})
		wr.objPopulate(true);	
	},

	dbDLt : function(){
		sqlQuery = "DROP table IF EXISTS alarms";
		db.serialize(function(){
			db.run(sqlQuery);
			console.log("table alarms is dropped");
			wr.objPopulate(true);
		})
	},

	updDb : function(entry,val,key){
		db.serialize(function() {
			sqlQuery = 'UPDATE alarms SET '+entry+' = "'+val+'" WHERE rowid="'+key+'"';db.run(sqlQuery);wr.objPopulate(true);
			
			//db.close();
			console.log("toggling completed")
			
		})
	},

	objPopulate : function(send){
		//console.log("objPopulate")
		//var objL = 0

		db.serialize(function() {
			var obj = {}
			sqlQuery = 'SELECT rowid AS id, * FROM alarms';
			db.each(sqlQuery,function(err,row){
				key = parseInt(row.id)
				obj[key] = {
					key : key,
					hour : row.hour,
					min : row.min,
					repeat : row.repeat,
					days : JSON.parse(row.days),
					ring : row.ring,
					rdStObj : JSON.parse(row.rdStObj),
					volRamp : row.volRamp,
					GMT : row.GMT,
					clientDate : new Date(obj.clientDate)
				}
				//objL++
			},function(){
				wr.alarms = obj;
				if(send){
					//console.log("send")
					io.emit("alarmsDB",wr.alarms);
				} else {
					//do not send
				}
				//return wr.alarms,wr.alarmsL;
			})
		})
	},

	padfield:function(f){
	    return (f<10)? "0"+f : f
	  },

	size : function(obj) {
		var size = 0, key;
		for (key in obj) {
		    if (obj.hasOwnProperty(key)) size++;
		}
		console.log("size: "+ size)
		return size;
	},

	mod : function (hour, GMT,day) {
		//console.log("GMT: " + GMT)
		
		console.log("day: " + day)
		var newH = hour + GMT;
		var newD;

        if(newH< 0){
        	newH = 24 + GMT;
        	//if(){
        		newD = day - 1
        		if(newD < 0){
        			newD = 6
        		}
        	//}
        } else {
        	newD = day

        }

        console.log(newD)

        obj = {
        	hour : newH,
        	day : newD
		}

		//io.emit("GMT",GMT)
		return obj; false
  
	},


	currentTimeAndAlarmChk : function(send){
		wr.dateobj = new Date()
		var ctH = parseInt(wr.dateobj.getHours());
		var ctM = parseInt(wr.dateobj.getMinutes());
		var ctS = parseInt(wr.dateobj.getSeconds());
		var ctD = parseInt(wr.dateobj.getDay());
		var ctGMT = wr.dateobj.getTimezoneOffset()/60
		//var ct = this.padfield(ctH)+":"+this.padfield(ctM)+":"+this.padfield(ctS)
		//console.log(ct)
/*
		if(send){
			io.emit("currentTime",JSON.stringify(dateobj))
		};

*/		


		if(ctS == 0){
			for (var key in wr.alarms){
				var alarm = wr.alarms[key]
				var alarmObj = {
					key : alarm.key,
					hour : alarm.hour,
					min : alarm.min,
					repeat : alarm.repeat,
					days : alarm.days,
					ring : alarm.ring,
					rdStObj : alarm.rdStObj,
					volRamp : alarm.volRamp,
					GMT : alarm.GMT,
					//clientDate : new Date(alarm.clientDate)
				}

				var alrmGMT = wr.mod(alarmObj.hour,alarmObj.GMT,undefined);
				var serverGMT = wr.mod(ctH,ctGMT,ctD);
				console.log(serverGMT)
				console.log(alarmObj.days.indexOf(serverGMT.day))

				if( alrmGMT.hour == serverGMT.hour && alarmObj.min == ctM){
					console.log("hi horse")
					if(alarmObj.days.indexOf(serverGMT.day) > -1){
						console.log("hi horse")
						if(alarmObj.ring == "true"){
							console.log("hi horse")
							if(alarmObj.repeat == "false"){
								wr.updDb("ring",'false',key)
							}
							console.log("ring ring 2");
							//wr.playThisAlrm = chk;
							io.emit("playThisAlrm",alarmObj)
						}
					}
				}					
			}
		}
	},
}
