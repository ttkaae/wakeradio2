var socket = io.connect(location.origin);

var audioCtx = new (window.AudioContext || window.webkitAudioContext)();
var button = document.querySelector('button');
var pre = document.querySelector('pre');
var myScript = document.querySelector('script');

pre.innerHTML = myScript.innerHTML;

// Stereo
var channels = 2;
// Create an empty two second stereo buffer at the
// sample rate of the AudioContext
var frameCount = audioCtx.sampleRate * 2.0;

var myArrayBuffer = audioCtx.createBuffer(channels, frameCount, audioCtx.sampleRate);

//button.onclick 

sound = function() {
  // Fill the buffer with white noise;
  //just random values between -1.0 and 1.0
  for (var channel = 0; channel < channels; channel++) {
   // This gives us the actual array that contains the data
   var nowBuffering = myArrayBuffer.getChannelData(channel);
   for (var i = 0; i < frameCount; i++) {
     // Math.random() is in [0; 1.0]
     // audio needs to be in [-1.0; 1.0]
     nowBuffering[i] = Math.random() * 2 - 1;
   }
  }

  
  // Get an AudioBufferSourceNode.
  // This is the AudioNode to use when we want to play an AudioBuffer
  var source = audioCtx.createBufferSource();
  // set the buffer in the AudioBufferSourceNode
  source.buffer = myArrayBuffer;
  // connect the AudioBufferSourceNode to the
  // destination so we can hear the sound
  source.connect(audioCtx.destination);
  // start the source playing
  source.start();
}

socket.on("soundA",function(){
	console.log("click")
	sound()
})


/*
var wakeradio = angular.module('wakeradio', []);
var socket = io.connect(location.origin);
var digits = 2


var wr = {
	player : document.getElementById("player"),
	timeRamp : 30, //Seconds
	fallback : {
		rdStObj : {index : 3, rdSt : "Spring.mp3", rdStURL : "assets/music/zedge/spring.mp3",volInit : 0.2},
		volRamp : "true",
	}

};

wr.player.addEventListener('error', function failed(e) {
   // audio playback failed - show a message saying why
   // to get the source of the audio element use $(this).src

   wr.playFallback(wr.fallback);

   switch (e.target.error.code) {
     case e.target.error.MEDIA_ERR_ABORTED:
       console.log('You aborted the video playback.');
       break;
     case e.target.error.MEDIA_ERR_NETWORK:
       console.log('A network error caused the audio download to fail.');
       break;
     case e.target.error.MEDIA_ERR_DECODE:
       console.log('The audio playback was aborted due to a corruption problem or because the video used features your browser did not support.');
       break;
     case e.target.error.MEDIA_ERR_SRC_NOT_SUPPORTED:
       console.log('The video audio not be loaded, either because the server or network failed or because the format is not supported.');
       break;
     default:
       console.log('An unknown error occurred.');
       break;
   }
 }, true);



wr.playFallback = function(obj){
   	wr.player.setAttribute("src",obj.rdStObj.rdStURL);
   	wr.rampUp(obj)
   	wr.player.play()
}


socket.on("pause",function(){
	wr.player.pause()
	console.log("pause")
});

socket.on("playBtn",function(data){
	var playObj = data
	var stream = playObj.rdStObj.rdStURL
	var volInit = playObj.volInit
	wr.play(stream,volInit)
	setTimeout(function(){
		if(isNaN(wr.player.duration)){
			wr.playFallback(wr.fallback)
		}
	},5000)
})



wakeradio.controller('EventController', ['$scope', function($scope) {


	socket.on("volume",function(data){
		console.log(data)
		$scope.volume = data;
		wr.player.volume = parseFloat($scope.volume)
		$scope.$apply()
	})

	wr.play = function(stream,volInit){
		wr.player.volume = volInit;
		wr.player.setAttribute("src",stream);
		wr.player.play()
		console.log("play "+stream)
		$scope.volume = volInit
		$scope.$apply()
		
	}

	wr.rampUp = function(obj){
		var volInit = obj.rdStObj.volInit;
		$scope.volume = volInit;
		$scope.$apply()

		if(obj.volRamp == "true"){
			wr.player.volume = 0
			console.log("true")
			for (var i = 0; i < wr.timeRamp ; i++) {
			    setTimeout(function() {
			    	$scope.volume = volInit;
					$scope.$apply()
					var tuneUp = volInit / wr.timeRamp
					wr.player.volume += tuneUp
			    }, 1000 * i);
			}		
		} else {
			wr.player.volume = volInit
		}
	}



	socket.on("playThisAlrm",function(data){
		wr.playObj = data
		wr.stream = wr.playObj.rdStObj.rdStURL;
		wr.player.setAttribute("src",wr.stream);

		wr.rampUp(wr.playObj)
		wr.player.play()

	})


}])
*/