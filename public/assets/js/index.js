var socket = io.connect(location.origin);
var $svAlrms = document.getElementById("svAlrms");
var $ct = document.getElementById("ct"); //change to scope
var $btnVol = document.getElementById("btnVol")
var wr = {};
var wakeradio = angular.module('wakeradio', []);

//audiobuffer
var sound = function(){
  console.log("click")
  socket.emit('sound')
}
//

setInterval(function () {
    wr.clientDate = new Date()
    wr.clientGMT = wr.clientDate.getTimezoneOffset()/60
    wr.showCurrentTime(wr.clientDate)
    //console.log(wr.clientDate)
    var add1SecC = wr.clientDate.getTime()+1000
    wr.clientDate.setTime(add1SecC) // with respect to client. only for the purpose of the shown clock
    $ct.innerHTML = wr.ct
  },1000)

/*
socket.on("currentTime",function(data){
  
  wr.showCurrentTime(wr.clientDate)
  wr.showCurrentTime(wr.clientDate)
  $ct.innerHTML = wr.ct

  wr.clientGMT = wr.clientDate.getTimezoneOffset()/60
  //wr.clientDate.setHours(wr.clientDate.getHours() - wr.clientGMT)
  wr.serverDate = new Date(data)
})
*/

socket.on("rdStObj",function(data){
  wr.rdStObj(data)
});

socket.on("outputMsg",function(data){
  wr.outputMsg(data);
})

socket.on("alarmsDB",function(data){
 wr.alrmTbl(data)
})

socket.emit("clientLoad");

//Server
wr = {
  outputMsg : function(msg){
    console.log(msg);
  },

  weekdays : [
    {weekday : "Sunday" , index : 0, selected : false},
    {weekday : "Monday" , index : 1, selected : true},
    {weekday : "Tuesday" , index : 2, selected : true},
    {weekday : "Wednesday" , index : 3, selected : true},
    {weekday : "Thursday" , index : 4, selected : true},
    {weekday : "Friday" , index : 5, selected : true},
    {weekday : "Saturday" , index : 6, selected : false},
  ],

  ClassCol : {
    danger : "btn-danger",
    info : "btn-info",
    primary : "btn-primary",
  },

  alrmTbl : function(obj){
    console.log(obj)
    wr.tmpScopeAlrms = [];

  for (var key in obj){
      //Time
      var hour = this.padfield(obj[key].hour)
      var min = this.padfield(obj[key].min)
      var time = hour+ ":"+ min;
      var rdSt = obj[key].rdStObj.rdSt

      //Days
      var days = ''
      for (var j = 0 ; j < obj[key].days.length ; j++){
        var index = parseInt(obj[key].days[j]);
        days += wr.weekdays[index].weekday
        if(j < obj[key].days.length - 1){
          days += ", "
        }
      }

      //Toggle button
      var toggle = ''
      var btnClass = ''
      switch(obj[key].ring){
        case "true" : toggle = "on";btnClass = wr.ClassCol.primary
        break;

        case "false" : toggle = 'off';btnClass = wr.ClassCol.danger
        break;

        default : //attr disabled = true
        break;
     }

     //Inside DOM;
     //Delete button
     //rdSt

       wr.tmpScopeAlrms.push({
        key : parseInt(key),
        time : time,
        days : days,
        rdSt : rdSt,
        toggle : toggle,
        btnClass : btnClass,
      })

    }

    wr.tblPrint()


  },


  toggleAlrm : function(id){
    console.log("still works")
    socket.emit("dbToggleAlrm",id);
  },

  padfield:function(f){
      return (f<10)? "0"+f : f
    },

  showCurrentTime : function(dt) {
    var ctH = parseInt(dt.getHours());
    var ctM = parseInt(dt.getMinutes());
    var ctS = parseInt(dt.getSeconds());
    wr.ct = this.padfield(ctH)+":"+this.padfield(ctM)+":"+this.padfield(ctS)

    return wr.ct
  },

  alrmDlt : function(id){
    console.log("dlt this row")
    socket.emit("dltAlrmFromDb",id)
  },



  pause : function (){
    socket.emit("pause")
  },

  changeVolume : function(vol){
    console.log(vol)
    socket.emit("volume",vol)
  }



}

wakeradio.controller('EventController', ['$scope', function($scope) {


  wr.tblPrint = function(){
      $scope.alarms = wr.tmpScopeAlrms
      $scope.$apply()
     // return $scope.alarms
  }
  
    $scope.alrm = {};

    $scope.svAlrm = function() {

      var svAlrmObj = {
        hour : $scope.alrm.hour,
        min : $scope.alrm.min,
        repeat : $scope.alrm.repeat,
        days : [],
        ring : "true",
        rdStObj : $scope.alrm.rdStSel,
        volRamp : $scope.alrm.volRamp.toString(),
        GMT : wr.clientGMT,
        clientDate : wr.clientDate,
        //user :
        //devices : [],
      }

      console.log("GMT: " + svAlrmObj.GMT)
      console.log("clientDate: " + svAlrmObj.clientDate)

      var svRdy = true

      switch(true){ //Time checking
        case (svAlrmObj.hour == undefined || svAlrmObj.min == undefined) :
        wr.outputMsg("The time is undefined");
        svRdy = false;
        break;

        case (svAlrmObj.hour < 0 && svAlrmObj.hour > 23) :
        wr.outputMsg("Hours not allowed")
        svRdy = false
        break;

        case (svAlrmObj.min < 0 && svAlrmObj.min > 59) :
        wr.outputMsg("Minutes not allowed")
        svRdy = false
        break;

      }



      if (svAlrmObj.repeat == undefined || !svAlrmObj.repeat) { //checkboxes defaults to true and false but the above is on load
        svAlrmObj.repeat = false;
        var now = wr.clientDate.getHours()*3600 + wr.clientDate.getMinutes() * 60 + wr.clientDate.getSeconds()
        var alrmTime = svAlrmObj.hour*3600 + svAlrmObj.min*60
        console.log("now: " + now)
        console.log("alrmTime: " + alrmTime)

        var days = svAlrmObj.clientDate.getDay()

        if(now > alrmTime){
          console.log("alrm tomorrow")
          days = (days + 1) % 7 
        } else {
          console.log("alrm later today")
          //No need to change anything
        }

        svAlrmObj.days.push(days)
      } else {
          //check which days
          svAlrmObj.days = $scope.selection

      } 

      if (svRdy) {
        console.log(svAlrmObj.rdStObj)

          //send to db
          socket.emit("btnSave",svAlrmObj)
      }
    };

    $scope.dltTbl = function(){
      console.log("dbDlt")
      socket.emit("dbDLt");
    };

    $scope.weekdays = wr.weekdays
    $scope.selection = [];
    $scope.toggleAlrm = wr.toggleAlrm
    $scope.alrmDlt = wr.alrmDlt

    $btnVol.addEventListener("mouseup",function(){
      wr.changeVolume($scope.volume)
    })

    $btnVol.addEventListener("touchend",function(){
     wr.changeVolume($scope.volume)
    })

    $scope.toggleSelection = function toggleSelection(weekday) {
      var idx = $scope.selection.indexOf(weekday);
      
      // is currently selected
      if (idx > -1) {
        $scope.selection.splice(idx, 1);
      }
      
      // is newly selected
      else {
        $scope.selection.push(weekday);
      }
    };

    var weekdaysInit = function(obj){
      for (var key in obj){
        if(obj[key].selected){
          $scope.selection.push(obj[key].index);
        }
      }
    }

  wr.rdStObj = function(obj){
    $scope.rdStObj = obj;
    $scope.alrm.rdStSel = $scope.rdStObj[0]
    console.log($scope.rdStObj)
  }

  $scope.rdStSelFunction = function(index){
    index = parseInt(index)
    $scope.alrm.rdStSel = $scope.rdStObj[index]
    
  }

  $scope.playBtn = function(){
    var playObj = {
      volInit : parseFloat($scope.volume),
      rdStObj : $scope.alrm.rdStSel,
    }
    socket.emit("playBtn",playObj)
  }

  $scope.pause = wr.pause
  
  weekdaysInit($scope.weekdays);

  $scope.alrm.volRamp = true

  $scope.volume = 0.8
  

}]);


